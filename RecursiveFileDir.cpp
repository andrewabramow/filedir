/*

��� ����� �������

� ����� �������� ������ ���� ����������� ���������� ���������
�� ���������, �� ���� �������� ������ ���� ������, ���������
(� ������������), ������� ���������� �� ���������� ����. ���
����� � ����������� ���������� ����
std::filesystem::recursive_directory_iterator, �����������
�� ���� ����, ������� �� ����� �������� ����������.
�� ������ ���� �� ����� �������� ��������� ����, ��� �����
���� ��� �������, ��� � ������� ����. ��������, ���

for(auto& p: fs::recursive_directory_iterator("D:\\dir1"))
        std::cout << p.path() << '\n';

������� ������ ���� �� ���� ������ � ������������ �
���������� D:\dir1. 
����� p.path() ���������� ������ ����
std::filesystem::path, ������� ���������:

����������, ���� ��� ��� ������� ��� ������ ������
is_regular_file().

�������� ���������� ��� �������� ����� ��� ������ ������
extension().

��� ������ ������ compare() ����� �������� ���������� �
������������ �������: 
p.path().extension().compare(�.torrent�).

�������� ������-�������, ������� ��������� �� ���� ���� �
�������� ������� � ������� ���������� � ���� ������, �
���������� ������ ��� ������, ������� ����� ������
����������(). ���������� ����� ������� ����� ���������� ���:

auto recursiveGetFileNamesByExtension =
       [](std::filesystem::path path,
          const std::string extension)

*/

#include <iostream>
#include <filesystem>
//namespace fs = std::filesystem;

int main()
{
    auto recursiveGetFileNamesByExtension =
        [](std::filesystem::path path,
            const std::string extension) {
                std::list <std::filesystem::path> list;
                for (auto& p : std::filesystem::recursive_directory_iterator(path)) {
                    if (p.path().extension().compare(extension)==0) {
                        list.push_back(p.path().filename());
                    }
                }
                return list;
    };

    std::filesystem::path path{ "D://���������//��������" };
    std::string extension{ ".zip" };

    for (auto i : recursiveGetFileNamesByExtension
    (path, extension)) {
        std::cout << i <<"\n";
    }
}

